#! /usr/bin/bash
if [ -z "$1" ]
then
    echo "enter password"
    #stty echo sets silent read for passwd and verify
    stty -echo
    read  passwd
    echo "verify password"
    read  verify
    stty echo
    if [ "$verify" != "$passwd" ]
    then
        exit
    fi
else
    passwd=$1
fi

#requires apache2-utils
hash_salt=$(htpasswd -nb -B admin "$passwd" | cut -d ":" -f 2)
echo "passwd=$hash_salt" > .env

docker-compose up -d

rm .env